arch ?= x86_64
profile ?= debug
iso := build/ros-$(arch).iso
kernel := build/kernel-$(arch).bin
target ?= $(arch)-ros
ros := build/rust/$(target)/$(profile)/libros.a

linker_script := src/arch/$(arch)/linker.ld
grub_cfg := src/arch/$(arch)/grub.cfg
assembly_source_files := $(wildcard src/arch/$(arch)/*.asm)
assembly_object_files := $(patsubst src/arch/$(arch)/%.asm, \
	build/arch/$(arch)/%.o, $(assembly_source_files))

.PHONY: all clean run iso

all: $(kernel)

clean:
	@rm -r build

run: $(iso)
	@qemu-system-x86_64 -m 5G -cdrom $(iso)

iso: $(iso)

iso_root_dir := build/isofiles
$(iso): $(kernel) $(grub_cfg)
	@mkdir -p $(iso_root_dir)/boot/grub
	@cp $(kernel) $(iso_root_dir)/boot/kernel.bin
	@cp $(grub_cfg) $(iso_root_dir)/boot/grub
	@grub-mkrescue -d /usr/lib/grub/i386-pc -o $(iso) $(iso_root_dir) 2> /dev/null
	@rm -r $(iso_root_dir)

$(kernel): FORCE $(ros) $(assembly_object_files) $(linker_script)
	@ld -n -T $(linker_script) -o $(kernel) \
		$(assembly_object_files) $(ros)

$(ros): FORCE
ifeq ($(profile),debug)
	@cargo build
else
	@cargo build --profile $(profile)
endif

build/arch/$(arch)/%.o: src/arch/$(arch)/%.asm
	@mkdir -p $(shell dirname $@)
	@nasm -f elf64 $< -o $@

FORCE:
