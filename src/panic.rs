use core::panic::PanicInfo;

use crate::{arch::utils::exit, println};

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{info}");
    exit();
}
