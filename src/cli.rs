use ascii::AsciiStr;

pub struct CLIArgs {
    pub dump_info: bool,
}
impl CLIArgs {
    pub fn parse(args: &AsciiStr) -> Self {
        let dump_info_arg = AsciiStr::from_ascii(b"--dump-info").unwrap();
        let dump_info = args == dump_info_arg;
        Self { dump_info }
    }
}
