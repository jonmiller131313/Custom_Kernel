use core::{
    fmt::{self, Write},
    ptr::NonNull,
};

use spin::{Lazy, Mutex};
use volatile::VolatilePtr;
use x86_64::instructions::interrupts::without_interrupts;

use crate::arch::utils::disable_cursor;

static mut VGA_WRITER: Lazy<Mutex<Writer>> =
    Lazy::new(|| Mutex::new(Writer::new(ColorCode::new(Color::Yellow, Color::Black))));

#[allow(dead_code)]
#[repr(u8)]
#[derive(Clone, Copy)]
enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGray = 7,
    DarkGray = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    Pink = 13,
    Yellow = 14,
    White = 15,
}

#[repr(transparent)]
#[derive(Clone, Copy)]
struct ColorCode(u8);
impl ColorCode {
    fn new(fg: Color, bg: Color) -> Self {
        Self((bg as u8) << 4 | fg as u8)
    }
}
impl Default for ColorCode {
    fn default() -> Self {
        Self::new(Color::White, Color::Black)
    }
}

#[repr(C, packed)]
#[derive(Clone, Copy)]
struct ScreenChar {
    ascii: u8,
    color: ColorCode,
}
impl ScreenChar {
    fn new(mut ascii: u8, color: ColorCode) -> Self {
        if !char::from(ascii).is_ascii() {
            ascii = 0xFE;
        }
        Self { ascii, color }
    }
}

struct ScreenBuffer(VolatilePtr<'static, [[ScreenChar; Self::WIDTH]; Self::HEIGHT]>);
impl ScreenBuffer {
    const VGA_ADDR: usize = 0xB8000;
    const WIDTH: usize = 80;
    const HEIGHT: usize = 25;
    fn new() -> Self {
        disable_cursor();
        Self(unsafe { VolatilePtr::new(NonNull::new_unchecked(Self::VGA_ADDR as *mut _)) })
    }

    fn read(&self, row: usize, col: usize) -> &ScreenChar {
        assert!(row < Self::HEIGHT);
        assert!(col < Self::WIDTH);
        let buff_ref = unsafe { self.0.as_raw_ptr().as_ref() };
        &buff_ref[row][col]
    }

    fn write(&mut self, row: usize, col: usize, ch: ScreenChar) {
        assert!(row < Self::HEIGHT);
        assert!(col < Self::WIDTH);
        let buff_ref = unsafe { self.0.as_raw_ptr().as_mut() };
        buff_ref[row][col] = ch;
    }
}

struct Writer {
    col: usize,
    color: ColorCode,
    buffer: ScreenBuffer,
}
impl Writer {
    fn new(color: ColorCode) -> Self {
        Self {
            col: 0,
            color,
            buffer: ScreenBuffer::new(),
        }
    }

    pub fn write_string(&mut self, msg: &str) {
        msg.bytes().for_each(|b| self.write_byte(b));
    }

    pub fn write_byte(&mut self, byte: u8) {
        if byte == b'\n' {
            self.new_line();
            return;
        } else if self.col == ScreenBuffer::WIDTH {
            self.new_line();
        }

        let row = ScreenBuffer::HEIGHT - 1;
        self.buffer.write(row, self.col, ScreenChar::new(byte, self.color));

        self.col += 1;
    }

    fn new_line(&mut self) {
        for row in 1..ScreenBuffer::HEIGHT {
            for col in 0..ScreenBuffer::WIDTH {
                let &ch = self.buffer.read(row, col);
                self.buffer.write(row - 1, col, ch);
            }
        }
        self.clear_row(ScreenBuffer::HEIGHT - 1);
        self.col = 0;
    }

    fn clear_row(&mut self, row: usize) {
        let blank = ScreenChar::new(b' ', self.color);
        for col in 0..ScreenBuffer::WIDTH {
            self.buffer.write(row, col, blank);
        }
    }
}
impl Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.write_string(s);
        Ok(())
    }
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::vga::_print(format_args!($($arg)*)));
}
#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)))
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    without_interrupts(|| unsafe { &VGA_WRITER }.lock().write_fmt(args).unwrap());
}
