//! See: https://www.gnu.org/software/grub/manual/multiboot2/multiboot.html#Boot-information-format

use core::{
    fmt::{Debug, Display, Formatter, Result},
    iter::Iterator,
    mem::{size_of, MaybeUninit},
    ptr::{addr_of_mut, NonNull},
    slice,
};

use ascii::AsciiStr;
use bitflags::bitflags;

use crate::arch::{elf, PhysAddrType};

type ByteBuff = &'static [u8];
type Str = &'static AsciiStr;

const ALIGNMENT: usize = 8;

pub struct Multiboot2Info {
    pub addr_range: (PhysAddrType, PhysAddrType),
    pub args: Str,
    pub bootloader: Str,
    pub basic_mem_info: BasicMemoryInfo,
    pub mem_maps: MemoryMap,
    pub elf_headers: ELFSymbols,
}
impl Multiboot2Info {
    pub unsafe fn new(multiboot_info: NonNull<u8>) -> Self {
        let parser = unsafe { Multiboot2InfoParser::parse(multiboot_info) };
        let mut new = MaybeUninit::uninit();
        let ptr: *mut Self = new.as_mut_ptr();

        let addr_start = PhysAddrType::new(multiboot_info.as_ptr() as _);
        let total_size: u32 = unsafe { *multiboot_info.as_ptr().cast() };
        let addr_end = addr_start + total_size as usize;
        unsafe { addr_of_mut!((*ptr).addr_range).write((addr_start, addr_end)) };

        for tag in parser {
            use Tag::*;
            unsafe {
                match tag {
                    CommandLine(line) => addr_of_mut!((*ptr).args).write(line),
                    Name(name) => addr_of_mut!((*ptr).bootloader).write(name),
                    MemInfo(info) => addr_of_mut!((*ptr).basic_mem_info).write(info),
                    MemMap(maps) => addr_of_mut!((*ptr).mem_maps).write(maps),
                    ELF(elf) => addr_of_mut!((*ptr).elf_headers).write(elf),
                }
            }
        }

        unsafe { new.assume_init() }
    }

    pub fn total_memory(&self) -> usize {
        self.mem_maps.iter_ram().map(|entry| entry.len as usize).sum()
    }

    pub fn kernel_addr_range(&self) -> (PhysAddrType, PhysAddrType) {
        let start = self
            .elf_headers
            .iter_kernel_sections()
            .map(|&elf::SectionHeader { addr, .. }| addr)
            .min()
            .unwrap();
        let end = self
            .elf_headers
            .iter_kernel_sections()
            .map(|&elf::SectionHeader { addr, size, .. }| addr + size)
            .max()
            .unwrap();
        (start, end)
    }
}
impl Debug for Multiboot2Info {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        writeln!(f, "Bootloader: {}", self.bootloader)?;
        writeln!(f, "Args: \"{}\"", self.args)?;
        writeln!(f, "{:?}", self.basic_mem_info)?;
        writeln!(f, "{:?}", self.mem_maps)?;
        write!(f, "{:?}", self.elf_headers)
    }
}

#[derive(Clone, Copy)]
struct Multiboot2InfoParser(ByteBuff);
impl Multiboot2InfoParser {
    /// Must be a valid address passed from the bootloader to the info struct.
    pub unsafe fn parse(multiboot_info: NonNull<u8>) -> Self {
        let size = unsafe { *multiboot_info.cast::<u32>().as_ref() };
        let mut buff = unsafe { slice::from_raw_parts(multiboot_info.as_ptr(), size as _) };

        // Skip size and reserved bytes
        skip_bytes(&mut buff, size_of::<u32>() * 2);

        Self(buff)
    }
}

#[non_exhaustive]
enum Tag {
    CommandLine(Str),
    Name(Str),
    MemInfo(BasicMemoryInfo),
    MemMap(MemoryMap),
    ELF(ELFSymbols), // Leave non-exhaustive to add the rest at a later date.
}

pub struct BasicMemoryInfo {
    pub mem_lower: u32,
    pub mem_upper: u32,
}
impl Debug for BasicMemoryInfo {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(
            f,
            "Memory Info {{ mem_lower: {:#02X}, mem_upper: {:#02X} }}",
            self.mem_lower, self.mem_upper
        )
    }
}

pub struct MemoryMap(&'static [MemoryMapEntry]);
impl MemoryMap {
    pub fn iter_ram(&self) -> RAMAreaIter {
        RAMAreaIter::new(self.0)
    }
}

pub struct RAMAreaIter {
    areas: &'static [MemoryMapEntry],
    i: usize,
}
impl RAMAreaIter {
    fn new(areas: &'static [MemoryMapEntry]) -> Self {
        assert!(!areas.is_empty());
        Self { areas, i: 0 }
    }
}
impl Iterator for RAMAreaIter {
    type Item = &'static MemoryMapEntry;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(area) = self.areas.get(self.i) {
            self.i += 1;
            let &MemoryMapEntry { type_, .. } = area;
            if type_ == MemoryType::RAM {
                return Some(area);
            }
        }
        None
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (0, Some(self.areas.len() - self.i))
    }
}

#[repr(C, packed)]
pub struct MemoryMapEntry {
    pub base_addr: PhysAddrType,
    pub len: u64,
    pub type_: MemoryType,
    _padding: u32,
}
impl MemoryMapEntry {
    const _SIZE_OK: () = assert!(
        size_of::<PhysAddrType>() == size_of::<u64>(),
        "Physical address type must have equivalent representation/size as u64"
    );
}
impl Debug for MemoryMap {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        writeln!(f, "Memory Maps [")?;
        for &MemoryMapEntry {
            type_, base_addr, len, ..
        } in self.iter_ram()
        {
            writeln!(f, "  {{ type: {type_}, addr: {base_addr:>#018X}, len: {len:#010X} }}")?;
        }
        write!(f, "]")
    }
}

bitflags! {
    #[repr(transparent)]
    #[derive(Clone, Copy, Eq, PartialEq)]
    pub struct MemoryType: u32 {
        const RAM = 1;
        const ACPIInfo = 3;
        const Hibernation = 4;
        const DefectiveRAM = 5;

        const Reserved = !0;
    }
}
impl Display for MemoryType {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        if let Some((name, _)) = self.iter_names().next() {
            write!(f, "{name}")
        } else {
            write!(f, "Reserved({})", self.bits())
        }
    }
}

pub struct ELFSymbols {
    section_name_table: &'static elf::SectionHeader,
    sections: &'static [elf::SectionHeader],
}
impl ELFSymbols {
    pub fn iter_kernel_sections(&self) -> impl Iterator<Item = &elf::SectionHeader> {
        self.sections
            .iter()
            .filter(|&&elf::SectionHeader { addr, .. }| addr != PhysAddrType::zero())
    }
}
impl Debug for ELFSymbols {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        writeln!(f, "ELF Symbols [")?;
        for header in self.sections[..15].iter() {
            let mut name = header.get_name(self.section_name_table);
            if name.len() >= 10 {
                name = &name[..10];
            }
            writeln!(f, "  {name:>10}={header:?}")?;
        }
        write!(f, "]")
    }
}

impl Iterator for Multiboot2InfoParser {
    type Item = Tag;

    fn next(&mut self) -> Option<Self::Item> {
        Tag::parse(&mut self.0)
    }
}

impl Tag {
    const TYPE_END: u32 = 0;
    const TYPE_CMD_LINE: u32 = 1;
    const TYPE_BOOTLOADER_NAME: u32 = 2;

    fn parse(buff: &mut ByteBuff) -> Option<Self> {
        let type_ = parse_u32(buff);
        let size: usize = parse_u32(buff) as usize - size_of::<u32>() * 2;

        use Tag::*;
        match type_ {
            Self::TYPE_END => {
                skip_bytes(buff, size);
                None
            }
            Self::TYPE_CMD_LINE => Some(CommandLine(parse_str(buff, size))),
            Self::TYPE_BOOTLOADER_NAME => Some(Name(parse_str(buff, size))),
            BasicMemoryInfo::TYPE => Some(MemInfo(BasicMemoryInfo::parse(buff, size))),
            MemoryMap::TYPE => Some(MemMap(MemoryMap::parse(buff, size))),
            ELFSymbols::TYPE => Some(ELF(ELFSymbols::parse(buff, size))),
            _ => {
                skip_unknown_tag(buff, size);
                Self::parse(buff)
            }
        }
    }
}

fn skip_bytes(buff: &mut ByteBuff, len: usize) {
    let (_, rest) = buff.split_at(len);
    *buff = rest;
}

fn len_w_padding(len: usize) -> usize {
    let bytes_over = len % ALIGNMENT;
    len + ((ALIGNMENT - bytes_over) % ALIGNMENT)
}

fn skip_unknown_tag(buff: &mut ByteBuff, len: usize) {
    skip_bytes(buff, len_w_padding(len));
}

fn parse_u32(buff: &mut ByteBuff) -> u32 {
    const SIZE: usize = size_of::<u32>();
    assert!(buff.len() >= SIZE);
    let (bytes, rest) = buff.split_at(SIZE);
    *buff = rest;
    u32::from_ne_bytes(bytes.try_into().unwrap())
}

fn parse_u16(buff: &mut ByteBuff) -> u16 {
    const SIZE: usize = size_of::<u16>();
    assert!(buff.len() >= SIZE);
    let (bytes, rest) = buff.split_at(SIZE);
    *buff = rest;
    u16::from_ne_bytes(bytes.try_into().unwrap())
}

fn parse_str(buff: &mut ByteBuff, len: usize) -> Str {
    let (name, rest) = buff.split_at(len_w_padding(len));
    *buff = rest;
    AsciiStr::from_ascii(&name[..len - 1]).expect("Unable to parse bootloader ascii string")
}

trait Multiboot2TagParse {
    const TYPE: u32;

    fn parse(buff: &mut ByteBuff, size: usize) -> Self
    where
        Self: Sized;
}

impl Multiboot2TagParse for BasicMemoryInfo {
    const TYPE: u32 = 4;

    fn parse(buff: &mut ByteBuff, _: usize) -> Self
    where
        Self: Sized,
    {
        let mem_lower = parse_u32(buff);
        let mem_upper = parse_u32(buff);
        Self { mem_lower, mem_upper }
    }
}

impl Multiboot2TagParse for MemoryMap {
    const TYPE: u32 = 6;

    fn parse(buff: &mut ByteBuff, size: usize) -> Self
    where
        Self: Sized,
    {
        let entry_size = parse_u32(buff);
        let version = parse_u32(buff);

        assert_eq!(version, 0, "Newer Multiboot memory map info versions are not supported");
        assert_eq!(entry_size as usize, size_of::<MemoryMapEntry>());

        let num_entries = (size - size_of::<u32>() * 2) / entry_size as usize;

        let (entries, rest) = buff.split_at(num_entries * size_of::<MemoryMapEntry>());
        *buff = rest;

        let entries = unsafe { slice::from_raw_parts(entries.as_ptr() as *const _, num_entries) };
        Self(entries)
    }
}

impl Multiboot2TagParse for ELFSymbols {
    const TYPE: u32 = 9;

    fn parse(buff: &mut ByteBuff, size: usize) -> Self
    where
        Self: Sized,
    {
        // It appears that the ELF Symbols tag does not confirm to the specification from the URL.
        // Adapted from: https://github.com/rust-osdev/multiboot2/blob/05875b03ed4d7c408de156377aa8fe103ac832da/multiboot2/src/elf_sections.rs#L17-L24
        let padded_len = len_w_padding(size);

        let num_sections = parse_u32(buff) as usize;
        let entry_size = parse_u32(buff) as usize;
        assert_eq!(entry_size, size_of::<elf::SectionHeader>());

        let string_idx = parse_u32(buff) as usize;
        assert!(string_idx < num_sections);

        let (entries, rest) = buff.split_at(padded_len - 12);
        *buff = rest;

        let sections = &unsafe { slice::from_raw_parts(entries.as_ptr() as *const _, num_sections) }[1..];
        let section_name_table = &sections[string_idx - 1];

        Self {
            section_name_table,
            sections,
        }
    }
}
