use cfg_if::cfg_if;

cfg_if! {
    if #[cfg(target_arch = "x86_64")] {
        use ::x86_64::PhysAddr;

        pub use self::x86_64::*;

        mod x86_64;

        pub type PhysAddrType = PhysAddr;
    } else {
        core::compile_error!("Architecture not supported");
    }
}
