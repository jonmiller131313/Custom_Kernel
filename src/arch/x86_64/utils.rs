use x86_64::instructions::{hlt, interrupts::without_interrupts, port::Port};

use crate::print;

pub fn exit() -> ! {
    print!("EXITED");
    without_interrupts(hlt);
    unreachable!();
}

pub fn disable_cursor() {
    unsafe {
        Port::<u8>::new(0x3D4).write(0x0A);
        Port::<u8>::new(0x3D5).write(0x20);
    }
}
