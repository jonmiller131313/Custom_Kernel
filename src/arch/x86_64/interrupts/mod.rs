use spin::Lazy;
use x86_64::{
    instructions::{hlt, interrupts},
    structures::idt::InterruptDescriptorTable,
};

use super::gdt::DOUBLE_FAULT_IST_INDEX;

use self::handlers::*;

mod controllers;
mod handlers;

static IDT: Lazy<InterruptDescriptorTable> = Lazy::new(|| {
    let mut idt = InterruptDescriptorTable::new();

    idt.breakpoint.set_handler_fn(breakpoint_handler);

    unsafe {
        idt.double_fault
            .set_handler_fn(double_fault_handler)
            .set_stack_index(DOUBLE_FAULT_IST_INDEX as u16);
    }

    idt.page_fault.set_handler_fn(page_fault_handler);

    use InterruptIndex::*;
    idt[Timer as usize].set_handler_fn(timer_interrupt_handler);
    idt[Keyboard as usize].set_handler_fn(keyboard_interrupt_handler);

    idt
});

pub fn init() {
    IDT.load();
    controllers::init();
    interrupts::enable();
}

pub fn hlt_loop() -> ! {
    loop {
        hlt();
    }
}

#[derive(Clone, Copy, Debug)]
#[repr(u8)]
enum InterruptIndex {
    Timer = controllers::PIC_1_OFFSET,
    Keyboard,
}
