use pc_keyboard::{layouts::Us104Key, DecodedKey, HandleControl, Keyboard, ScancodeSet1};
use spin::{Lazy, Mutex};
use x86_64::{
    instructions::port::Port,
    registers::control::Cr2,
    structures::idt::{InterruptStackFrame, PageFaultErrorCode},
};

use crate::{print, println};

use super::{controllers::finish_interrupt, InterruptIndex};

pub(super) extern "x86-interrupt" fn breakpoint_handler(stack_frame: InterruptStackFrame) {
    println!("EXCEPTION: BREAKPOINT");
    println!("{stack_frame:#?}");
}

pub(super) extern "x86-interrupt" fn double_fault_handler(stack_frame: InterruptStackFrame, error_code: u64) -> ! {
    println!("EXCEPTION: DOUBLE FAULT: {error_code}");
    panic!("{stack_frame:#?}")
}

pub(super) extern "x86-interrupt" fn timer_interrupt_handler(_: InterruptStackFrame) {
    // print!(".");
    finish_interrupt(InterruptIndex::Timer as u8);
}

pub(super) extern "x86-interrupt" fn keyboard_interrupt_handler(_stack_frame: InterruptStackFrame) {
    static KEYBOARD: Lazy<Mutex<Keyboard<Us104Key, ScancodeSet1>>> =
        Lazy::new(|| Mutex::new(Keyboard::new(ScancodeSet1::new(), Us104Key, HandleControl::Ignore)));

    let mut keyboard = KEYBOARD.lock();
    let mut port = Port::new(0x60);
    let scancode = unsafe { port.read() };

    let key_event = keyboard
        .add_byte(scancode)
        .expect("Reading keyboard != Ok")
        .expect("Reading keyboard != Some");
    if let Some(DecodedKey::Unicode(ch)) = keyboard.process_keyevent(key_event) {
        print!("{ch}");
    };
    finish_interrupt(InterruptIndex::Keyboard as u8);
}

pub(super) extern "x86-interrupt" fn page_fault_handler(
    stack_frame: InterruptStackFrame,
    error_code: PageFaultErrorCode,
) {
    println!("EXCEPTION: PAGE FAULT");
    println!("Access address: {:?}", Cr2::read());
    println!("Error code: {error_code:?}");
    panic!("{stack_frame:#?}");
}
