use core::ops::DerefMut;
use spin::{Lazy, Mutex};
use x86_64::instructions::port::Port;

pub(super) const PIC_1_OFFSET: u8 = 32;
const PIC_2_OFFSET: u8 = PIC_1_OFFSET + 8;

const CMD_INIT: u8 = 0x11;
const CMD_ACK: u8 = 0x20;

static CONTROLLERS: Lazy<Mutex<Controllers>> = Lazy::new(Controllers::new);

pub(super) fn init() {
    // Some motherboards needs a small delay between writing to the controllers. Using a trick Linux uses we write
    // gibberish to the BIOS POST port.
    let mut wait_port: Port<u8> = Port::new(0x80);
    unsafe {
        let mut lock = CONTROLLERS.lock();
        let Controllers { primary, secondary } = lock.deref_mut();
        let mut wait = || wait_port.write(0);

        // Store masks
        let primary_mask = primary.data.read();
        let secondary_mask = secondary.data.read();

        // Initialize command
        primary.command.write(CMD_INIT);
        wait();
        secondary.command.write(CMD_INIT);
        wait();

        // Interrupt offsets
        primary.data.write(primary.offset);
        wait();
        secondary.data.write(secondary.offset);
        wait();

        // Chaining between PIC1 and PIC2
        primary.data.write(4);
        wait();
        secondary.data.write(2);
        wait();

        // Environment mode
        primary.data.write(1);
        wait();
        secondary.data.write(1);
        wait();

        // Restore masks
        primary.data.write(primary_mask);
        wait();
        secondary.data.write(secondary_mask);
        wait()
    }
}

pub(super) fn finish_interrupt(id: u8) {
    let mut lock = CONTROLLERS.lock();
    let Controllers { primary, secondary } = lock.deref_mut();

    if primary.handles_interrupt(id) {
        unsafe { primary.command.write(CMD_ACK) };
    } else if secondary.handles_interrupt(id) {
        unsafe { secondary.command.write(CMD_ACK) };
    } else {
        panic!("Unhandled interrupt: {id}");
    }
}

struct Controllers {
    primary: Controller,
    secondary: Controller,
}
impl Controllers {
    fn new() -> Mutex<Self> {
        Mutex::new(Self {
            primary: Controller::primary(),
            secondary: Controller::secondary(),
        })
    }
}

struct Controller {
    offset: u8,
    command: Port<u8>,
    data: Port<u8>,
}
impl Controller {
    fn primary() -> Self {
        Self {
            offset: PIC_1_OFFSET,
            command: Port::new(0x20),
            data: Port::new(0x21),
        }
    }

    fn secondary() -> Self {
        Self {
            offset: PIC_2_OFFSET,
            command: Port::new(0xA0),
            data: Port::new(0xA1),
        }
    }

    fn handles_interrupt(&self, id: u8) -> bool {
        self.offset <= id && id < self.offset + 8
    }
}
