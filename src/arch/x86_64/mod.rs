mod gdt;
mod interrupts;

pub mod elf;
pub mod utils;

pub use self::interrupts::hlt_loop;

pub fn init_hardware() {
    gdt::init();
    interrupts::init();
}
