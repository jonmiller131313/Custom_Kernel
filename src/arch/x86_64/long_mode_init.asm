global long_mode_start
extern rust_main

%assign VGA_BUFF 0xB8000

section .text
bits 64
long_mode_start:
    ; Zero out all data segment registers
    mov ax, 0
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    jmp rust_main
