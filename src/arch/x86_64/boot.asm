global start
extern long_mode_start

%assign VGA_BUFF 0xB8000


section .text
bits 32
start:
    mov esp, stack_top
    mov edi, ebx

    call check_multiboot
    call check_cpuid
    call check_long_mode

    call setup_page_tables
    call enable_paging

    ; Load the 64-bit GDT
    lgdt [gdt64.pointer]
    ; Enable 64-bit mode
    jmp gdt64.code:long_mode_start


check_multiboot:
    cmp eax, 0x36D76289
    jne .no_multiboot
    ret
.no_multiboot:
    mov al, "0"
    jmp error

check_cpuid:
    ; Check if CPUID is supported by attempting to flip the ID bit (bit 21) in the FLAGS register. If we can flip it,
    ; CPUID is available. See: https://wiki.osdev.org/Setting_Up_Long_Mode#Detection_of_CPUID

    ; Copy FLAGS in to EAX via stack
    pushfd
    pop eax

    ; Copy to ECX as well for comparing later on
    mov ecx, eax

    ; Flip the ID bit
    xor eax, 1 << 21

    ; Copy EAX to FLAGS via the stack
    push eax
    popfd

    ; Copy FLAGS back to EAX (with the flipped bit if CPUID is supported)
    pushfd
    pop eax

    ; Restore FLAGS from the old version stored in ECX (i.e. flipping the
    ; ID bit back if it was ever flipped).
    push ecx
    popfd

    ; Compare EAX and ECX. If they are equal then that means the bit
    ; wasn't flipped, and CPUID isn't supported.
    cmp eax, ecx
    je .no_cpuid
    ret
.no_cpuid:
    mov al, "1"
    jmp error

check_long_mode:
    ; Test if extended processor info in available
    mov eax, 0x80000000    ; Implicit argument for cpuid
    cpuid                  ; Get highest supported argument
    cmp eax, 0x80000001    ; It needs to be at least 0x80000001
    jb .no_long_mode       ; If it's less, the CPU is too old for long mode

    ; Use extended info to test if long mode is available
    mov eax, 0x80000001    ; Argument for extended processor info
    cpuid                  ; Returns various feature bits in ecx and edx
    test edx, 1 << 29      ; Test if the LM-bit is set in the D-register
    jz .no_long_mode       ; If it's not set, there is no long mode
    ret
.no_long_mode:
    mov al, "2"
    jmp error


setup_page_tables:
    ; Map first P4 entry to P3 table
    mov eax, p3_table
    or eax, 0b11 ; Present + writable
    mov [p4_table], eax

    ; Map first P3 entry to P2 table
    mov eax, p2_table
    or eax, 0b11 ; Present + writable
    mov [p3_table], eax

    ; Map each P2 entry to a huge 2MiB page
    mov ecx, 0          ; Counter variable

.map_p2_table:
    mov eax, 0x200000   ; 2MiB
    mul ecx             ; Start of ecx-th page
    or eax, 0x83        ; Present + writable + huge
    mov [p2_table + ecx * 8], eax ; Map the entry

    inc ecx             ; Increment counter
    cmp ecx, 512        ; 512 Entries in P2 table
    jne .map_p2_table   ; Loop

    ret

enable_paging:
    ; Load P4 address to cr3 register
    mov eax, p4_table
    mov cr3, eax

    ; Enable Physical Address Extension flag
    mov eax, cr4
    or eax, 1 << 5
    mov cr4, eax

    ; Set the long mode bit in the EFER Model Specific Register
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8
    wrmsr

    ; Enable paging in the cr0 register
    mov eax, cr0
    or eax, 1 << 31
    mov cr0, eax

    ret


error:
    mov dword [VGA_BUFF],      0x4F524F45
    mov dword [VGA_BUFF +  4], 0x4F3A4F52
    mov dword [VGA_BUFF +  8], 0x4F204F20
    mov byte  [VGA_BUFF + 10], al
    mov byte  [VGA_BUFF + 11], 0x4F
    hlt


section .bss
; Virtual memory identity mapping
; P4, 3, 2, 1 = PML4, PDP, PD, PT
align 4096
p4_table:
    resb 4096
p3_table:
    resb 4096
p2_table:
    resb 4096
p1_table:
    resb 4096

stack_bottom:
    resb 4096 * 4
stack_top:


section .rodata
gdt64:
    dq 0; Zero entry
.code: equ $ - gdt64
    dq (1 << 43) | (1 << 44) | (1 << 47) | (1 << 53) ; Code + code + valid + 64-bit
.pointer:
    dw $ - gdt64 - 1
    dq gdt64
