section .multiboot_header
%assign MULTIBOOT_HEADER 0xE85250D6
%assign MULTIBOOT_ARCH   0

header_start:
    dd MULTIBOOT_HEADER             ; Multiboot 2 Magic Number
    dd MULTIBOOT_ARCH               ; i386 Architecture
    dd header_end - header_start    ; Header length

    ; Checksum (First term avoids a compiler warning)
    dd 0x100000000 - (MULTIBOOT_HEADER + MULTIBOOT_ARCH + (header_end - header_start))

    ; End tag
    dw 0
    dw 0
    dd 8
header_end:
