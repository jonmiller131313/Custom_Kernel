//! See:
//!  - https://man.archlinux.org/man/elf.5
//!  - https://github.com/torvalds/linux/blob/master/include/uapi/linux/elf.h

use core::{
    ffi::CStr,
    fmt::{Debug, Formatter, Result},
    mem::transmute,
    num::{NonZeroU32, NonZeroUsize},
    ops::RangeInclusive,
    slice,
};

use ascii::AsciiStr;
use bitflags::bitflags;
use x86_64::PhysAddr;

#[repr(C, packed)]
pub struct SectionHeader {
    // Index into the section header string table section, offset of a CString.
    pub name_off: u32,
    pub type_: SectionContents,
    pub flags: SectionFlags,

    // Where the first byte of the section _should_ reside, or NULL
    pub addr: PhysAddr,

    // The file offset of the first byte of the section, for ElfSectionType::NotPresent it's the conceptual placement in
    // the file.
    pub offset: usize,

    // The number of bytes in the section (ElfSectionType::NotPresent may be nonzero)
    pub size: u64,

    // Interpretation depends on section type
    pub link: u32,
    // Interpretation depends on section type
    pub info: u32,

    // The value of alignment for the section (including addr), 0 or 1 if it does not require alignment.
    pub addr_align: u64,

    // Holds the size if the section contains fixed sized entries, 0 otherwise.
    pub ent_size: u64,
}
impl SectionHeader {
    pub fn get_name(&self, name_table: &Self) -> &AsciiStr {
        let &Self { name_off, .. } = self;
        let &Self { type_, size, .. } = name_table;
        assert_eq!(type_, SectionContents::String);

        let name = name_table.addr.as_u64() as *const u8;
        let str_data = unsafe { slice::from_raw_parts(name, size as _) };

        let name = CStr::from_bytes_until_nul(&str_data[name_off as _..]).unwrap();

        AsciiStr::from_ascii(name.to_bytes()).unwrap()
    }
}
impl Debug for SectionHeader {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        let &Self { addr, size, flags, .. } = self;
        write!(
            f,
            "{{ addr: {addr:>#018X}, len: {size:>#010X}, flags: {:>#04X?} }}",
            flags.bits()
        )
    }
}

bitflags! {
    #[repr(transparent)]
    #[derive(Clone, Copy, Eq, PartialEq)]
    pub struct SectionContents: u32 {
        const Null = 0;
        const Program = 1;
        const Symbol = 2;
        const String = 3;
        const RelocateA = 4;
        const Hash = 5;
        const Dynamic = 6;
        const Notes = 7;
        const NotPresent = 8;
        const Relocate = 9;
        const Reserved = 10; // SHT_SHLIB
        const DynamicSymbols = 11;

        const Range = !0;
    }

    #[repr(transparent)]
    #[derive(Clone, Copy)]
    pub struct SectionFlags: u64 {
        const Write = 1;
        const Memory = 2;
        const Exec = 4;
        const Processor = 0xF0000000;

        const Unknown = !0;
    }

    #[repr(transparent)]
    #[derive(Clone, Copy)]
    pub struct SectionBinding: u8 {
        const Local = 0;
        const Global = 1;
        const Weak = 2;

        const _ = !0;
    }
}
impl Debug for SectionContents {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        const OS_RANGE: RangeInclusive<u32> = 0x60000000..=0x6FFFFFFF;
        const PROC_RANGE: RangeInclusive<u32> = 0x70000000..=0x7FFFFFFF;
        const USER_RANGE: RangeInclusive<u32> = 0x80000000..=0xFFFFFFFF;

        if let Some((name, _)) = self.iter_names().next() {
            return write!(f, "{name}");
        }

        write!(
            f,
            "{}({})",
            match self.bits() {
                os if OS_RANGE.contains(&os) => "OS",
                proc if PROC_RANGE.contains(&proc) => "Proc",
                user if USER_RANGE.contains(&user) => "User",
                _ => "Unknown",
            },
            self.bits()
        )
    }
}
impl Debug for SectionFlags {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        if let Some((name, _)) = self.iter_names().next() {
            write!(f, "{name}")
        } else {
            write!(f, "Unknown({})", self.bits())
        }
    }
}
impl Debug for SectionBinding {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        if let Some((name, _)) = self.iter_names().next() {
            write!(f, "{name}")
        } else {
            write!(f, "Unknown({})", self.bits())
        }
    }
}

#[repr(transparent)]
#[derive(Clone, Copy)]
pub struct SectionInfo(u8);
impl SectionInfo {
    pub fn unpack(&self) -> (SectionType, SectionBinding) {
        let info = self.0;
        unsafe { (transmute(info & 0xF), transmute(info >> 4)) }
    }
}

#[repr(u8)]
#[derive(Clone, Copy, Debug)]
pub enum SectionType {
    NoType,
    Object,
    Exec,
    Section,
    File,
    Common,
    Tls,
}

pub struct Symbol {
    // Some do not have a name
    pub name: Option<NonZeroU32>,
    pub info: SectionInfo,
    pub other: u8,
    pub shndx: u16,
    pub value: PhysAddr,
    // Some do not have a size or an unknown size
    pub size: Option<NonZeroUsize>,
}
