#![no_std]
#![feature(abi_x86_interrupt)]

use core::ptr::NonNull;

use crate::{
    arch::{hlt_loop, init_hardware},
    cli::CLIArgs,
    multiboot2::Multiboot2Info,
};

mod arch;
mod cli;
mod memory;
mod multiboot2;
mod panic;
mod vga;

#[no_mangle]
pub extern "C" fn rust_main(multiboot_info: NonNull<u8>) -> ! {
    let info = unsafe { Multiboot2Info::new(multiboot_info) };
    let args = CLIArgs::parse(info.args);

    if args.dump_info {
        println!("{info:?}");
    }

    println!("Hello world!");

    init_hardware();

    hlt_loop()
}
