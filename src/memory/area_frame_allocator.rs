use core::iter::Iterator;
use core::ops::RangeInclusive;

use crate::multiboot2::{MemoryMapEntry, Multiboot2Info, RAMAreaIter};

use super::*;

pub struct AreaFrameAllocator {
    next_free_frame: Frame,
    current_area: Option<&'static MemoryMapEntry>,
    areas: RAMAreaIter,
    kernel_addr_range: RangeInclusive<Frame>,
    boot_info_addr_range: RangeInclusive<Frame>,
}
impl AreaFrameAllocator {
    pub fn new(info: &Multiboot2Info) -> Self {
        let (multiboot_start, multiboot_end) = info.addr_range;
        let (kernel_start, kernel_end) = info.kernel_addr_range();
        let next_free_frame = Frame::zero();
        let mut areas = info.mem_maps.iter_ram();
        let current_area = areas.next();

        Self {
            next_free_frame,
            current_area,
            areas,
            kernel_addr_range: kernel_start.into()..=(kernel_end - 1u64).into(),
            boot_info_addr_range: multiboot_start.into()..=(multiboot_end - 1u64).into(),
        }
    }

    fn update_next_area(&mut self) {
        let old_addr_start = self
            .current_area
            .map(|a| a.base_addr)
            .unwrap_or_else(PhysAddrType::zero);
        self.current_area = self.areas.next();
        if let Some(&MemoryMapEntry { base_addr, .. }) = self.current_area {
            assert!(base_addr > old_addr_start);
            self.next_free_frame = base_addr.into();
        }
    }
}

impl FrameAllocator for AreaFrameAllocator {
    fn allocate_frame(&mut self) -> Option<Frame> {
        let area = self.current_area?;

        let new_frame = self.next_free_frame;
        let last_valid_addr = area.base_addr + area.len - 1u64;
        let current_area_last_frame = Frame::from(last_valid_addr);

        if new_frame > current_area_last_frame
            || ((last_valid_addr.as_u64() as usize + 1) % PAGE_SIZE != 0 && new_frame == current_area_last_frame)
        {
            // Second check means we don't use the last frame of mem area if the end addr is not page aligned.
            self.update_next_area();
        } else if self.kernel_addr_range.contains(&new_frame) {
            self.next_free_frame = *self.kernel_addr_range.end() + 1;
        } else if self.boot_info_addr_range.contains(&new_frame) {
            self.next_free_frame = *self.boot_info_addr_range.end() + 1;
        } else {
            self.next_free_frame += 1;
            return Some(new_frame);
        }

        self.allocate_frame()
    }

    fn deallocate_frame(&mut self) {
        unimplemented!()
    }
}
