use core::ops::{Add, AddAssign};

use crate::arch::PhysAddrType;

mod area_frame_allocator;

pub use self::area_frame_allocator::AreaFrameAllocator;

const PAGE_SIZE: usize = 4098;

#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Frame(usize);
impl Frame {
    fn zero() -> Self {
        Self(0)
    }
}
impl From<PhysAddrType> for Frame {
    fn from(addr: PhysAddrType) -> Self {
        Self(addr.as_u64() as usize / PAGE_SIZE)
    }
}
impl Add<usize> for Frame {
    type Output = Self;

    fn add(mut self, rhs: usize) -> Self::Output {
        self.0 += rhs;
        self
    }
}
impl AddAssign<usize> for Frame {
    fn add_assign(&mut self, rhs: usize) {
        self.0 += rhs;
    }
}

pub trait FrameAllocator {
    fn allocate_frame(&mut self) -> Option<Frame>;

    fn deallocate_frame(&mut self);
}
